package com.ojeda.app.wings.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ojeda.app.wings.models.Avion;

public interface IAvionesDao extends JpaRepository<Avion, Long>{

}
