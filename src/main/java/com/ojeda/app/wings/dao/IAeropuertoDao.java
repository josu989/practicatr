package com.ojeda.app.wings.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ojeda.app.wings.models.Aeropuerto;
import com.ojeda.app.wings.models.Avion;

public interface IAeropuertoDao extends JpaRepository<Aeropuerto, Long>{

}
