package com.ojeda.app.wings.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ojeda.app.wings.models.Piloto;

public interface IPilotosDao extends JpaRepository<Piloto, Long>{

}
