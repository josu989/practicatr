package com.ojeda.app.wings.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ojeda.app.wings.dao.IAvionesDao;
import com.ojeda.app.wings.dao.IPilotosDao;
import com.ojeda.app.wings.dtos.AvionCreateDTO;
import com.ojeda.app.wings.dtos.AvionListDTO;
import com.ojeda.app.wings.dtos.PilotoCreateDTO;
import com.ojeda.app.wings.dtos.PilotoListDTO;
import com.ojeda.app.wings.models.Avion;
import com.ojeda.app.wings.models.Piloto;



@Service
public class AvionService implements IService<AvionListDTO,AvionCreateDTO>{

	@Autowired
	private IAvionesDao avionesDao;
	
	@Override
	public AvionListDTO create(AvionCreateDTO avionDTO) {
		Avion avion = this.dtoToEntity(avionDTO);
		Avion avionNuevo = avionesDao.save(avion);
		return this.entityToDto(avionNuevo);
	}

	@Override
	public List<AvionListDTO> getAll() {
		List<Avion> listaAviones = avionesDao.findAll();
		List<AvionListDTO> contenido = listaAviones.stream().map(avion -> this.entityToDto(avion)).collect(Collectors.toList());
		return contenido;
	}

	@Override
	public AvionListDTO getById(long id) {
		Avion avion = avionesDao.findById(id).orElseThrow(() -> new RuntimeException());
		return this.entityToDto(avion);
	}

	@Override
	public AvionListDTO update(AvionCreateDTO avionDTO, long id) {
		Avion avion = avionesDao.findById(id).orElseThrow(() -> new RuntimeException());
		avion.setId(avionDTO.getId());
		avion.setCodigoAvion(avionDTO.getCodigoAvion());
		avion.setTipoAvion(avionDTO.getCodigoAvion());
		avion.setHoraVuelo(avionDTO.getHoraVuelo());
		avion.setCapacidadPasajeros(avionDTO.getCapacidadPasajeros());
		Avion avionActualizado = avionesDao.save(avion);
		return this.entityToDto(avionActualizado);
	}

	@Override
	public void delete(long id) {
		Avion avion = avionesDao.findById(id).orElseThrow(() -> new RuntimeException());
		avionesDao.delete(avion);
		
	}
	
	public Avion dtoToEntity(AvionCreateDTO avionDTO) {
		Avion avion = new Avion();
		avion.setId(avionDTO.getId());
		avion.setCodigoAvion(avionDTO.getCodigoAvion());
		avion.setTipoAvion(avionDTO.getTipoAvion());
		avion.setHoraVuelo(avionDTO.getHoraVuelo());
		avion.setCapacidadPasajeros(avionDTO.getCapacidadPasajeros());

		return avion;
	}
	
public AvionListDTO entityToDto(Avion avion) {
		
		AvionListDTO avionDTO = new AvionListDTO();
		
		avionDTO.setId(avion.getId());
		avionDTO.setCodigoAvion(avion.getCodigoAvion());
		avionDTO.setTipoAvion(avion.getTipoAvion());
		avionDTO.setHoraVuelo(avion.getHoraVuelo());
		avionDTO.setCapacidadPasajeros(avion.getCapacidadPasajeros());
	
		return avionDTO;
	}
	

}
