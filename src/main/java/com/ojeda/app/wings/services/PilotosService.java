package com.ojeda.app.wings.services;

import com.ojeda.app.wings.dtos.PilotoListDTO;
import com.ojeda.app.wings.models.Piloto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ojeda.app.wings.dao.IPilotosDao;
import com.ojeda.app.wings.dtos.PilotoCreateDTO;


@Service
public class PilotosService implements IService<PilotoListDTO,PilotoCreateDTO>{

	
	@Autowired
	private IPilotosDao pilotosDao;

	@Override
	public PilotoListDTO create(PilotoCreateDTO pilotoDTO) {
		Piloto piloto = this.dtoToEntity(pilotoDTO);
		Piloto pilotoNuevo = pilotosDao.save(piloto);
		return this.entityToDto(pilotoNuevo);
	}

	@Override
	public List<PilotoListDTO> getAll() {
		List<Piloto> listaPilotos = pilotosDao.findAll();
		List<PilotoListDTO> contenido = listaPilotos.stream().map(piloto -> this.entityToDto(piloto)).collect(Collectors.toList());
		return contenido;
	}

	@Override
	public PilotoListDTO getById(long id) {
		Piloto piloto = pilotosDao.findById(id).orElseThrow(() -> new RuntimeException());
		return this.entityToDto(piloto);
	}

	@Override
	public PilotoListDTO update(PilotoCreateDTO pilotoDTO, long id) {
		Piloto piloto = pilotosDao.findById(id).orElseThrow(() -> new RuntimeException());
		piloto.setId(pilotoDTO.getId());
		piloto.setNombre(pilotoDTO.getNombre());
		piloto.setAp_paterno(pilotoDTO.getAp_paterno());
		piloto.setAp_materno(pilotoDTO.getAp_materno());
		piloto.setGenero(pilotoDTO.getGenero());
		Piloto pilotoActualizado = pilotosDao.save(piloto);
		return this.entityToDto(pilotoActualizado);
	}

	@Override
	public void delete(long id) {
		Piloto piloto = pilotosDao.findById(id).orElseThrow(() -> new RuntimeException());
		pilotosDao.delete(piloto);
		
	}
	
	public Piloto dtoToEntity(PilotoCreateDTO pilotoDTO) {
		Piloto piloto = new Piloto();
		piloto.setId(pilotoDTO.getId());
		piloto.setNombre(pilotoDTO.getNombre());
		piloto.setAp_paterno(pilotoDTO.getAp_paterno());
		piloto.setAp_materno(pilotoDTO.getAp_materno());
		piloto.setGenero(pilotoDTO.getGenero());
		piloto.setHorasVuelo(pilotoDTO.getHorasVuelo());

		return piloto;
	}
	
	
	public PilotoListDTO entityToDto(Piloto piloto) {
		
		PilotoListDTO pilotoDTO = new PilotoListDTO();
		
		pilotoDTO.setId(piloto.getId());
		pilotoDTO.setNombre(piloto.getNombre());
		pilotoDTO.setAp_paterno(piloto.getAp_paterno());
		pilotoDTO.setAp_materno(piloto.getAp_materno());
		pilotoDTO.setGenero(piloto.getGenero());
		pilotoDTO.setHorasVuelo(piloto.getHorasVuelo());
	
		return pilotoDTO;
	}
	
	
}
