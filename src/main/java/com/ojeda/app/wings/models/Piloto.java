package com.ojeda.app.wings.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity //entidad
@Table(name="pilotos") //lo pasamos como tabla
public class Piloto {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ")
	@SequenceGenerator(sequenceName = "customer_seq", allocationSize = 1, name = "CUST_SEQ")
	
	
	@Column(name="id_piloto")  //declaramos los atributos y los pasamos como columnas 
	private Long id;
	
	@Column(name="codigoPiloto")  //declaramos los atributos y los pasamos como columnas 
	private String codigo_piloto;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="ap_paterno")
	private String ap_paterno;
	
	@Column(name="ap_materno")
	private String ap_materno;
	
	@Column(name="genero")
	private String genero;
	
	@Column(name="hora_vuelo")
	private Integer horasVuelo;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo_piloto() {
		return codigo_piloto;
	}

	public void setCodigo_piloto(String codigo_piloto) {
		this.codigo_piloto = codigo_piloto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAp_paterno() {
		return ap_paterno;
	}

	public void setAp_paterno(String ap_paterno) {
		this.ap_paterno = ap_paterno;
	}

	public String getAp_materno() {
		return ap_materno;
	}

	public void setAp_materno(String ap_materno) {
		this.ap_materno = ap_materno;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Integer getHorasVuelo() {
		return horasVuelo;
	}

	public void setHorasVuelo(Integer horasVuelo) {
		this.horasVuelo = horasVuelo;
	}

	
	//constructor
	public Piloto() {
		super();
	}
	
	
	
	
	
}
