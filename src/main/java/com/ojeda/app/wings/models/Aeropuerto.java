package com.ojeda.app.wings.models;

import java.util.Date;
import java.time.*;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity //entidad
@Table(name="aeropuertos") //lo pasamos como tabla
public class Aeropuerto {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ3")
	@SequenceGenerator(sequenceName = "customer_seq3", allocationSize = 1, name = "CUST_SEQ3")
	
	@Column(name="id_aeropuerto")  //declaramos los atributos y los pasamos como columnas 
	private Long id;
	
	@Column (name="nombre")
	private String nombre;
	
	@Column (name="municipio")
	private String municipio;
	
	@Column (name="estado")
	private String estado;

	@Column (name="pais")
	private String pais;
	
	@Column (name="horas_vuelo_destino")
	private Date horasVueloDestino;
	
	

	public Aeropuerto() {
		super();
	}

	
	public Date getHorasVueloDestino() {
		return horasVueloDestino;
	}

	public void setHorasVueloDestino(Date horasVueloDestino) {
		this.horasVueloDestino = horasVueloDestino;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
}
