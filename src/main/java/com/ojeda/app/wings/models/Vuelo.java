package com.ojeda.app.wings.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity //entidad
@Table(name="vuelos") //lo pasamos como tabla
public class Vuelo {
	
	@Id
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ4")
	@SequenceGenerator(sequenceName = "customer_seq4", allocationSize = 1, name = "CUST_SEQ4")
	
	@Column(name="id_vuelo")
	private Long id;

	@Column(name="numero_vuelo")
	private String numeroVuelo;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)// un vuelo tiene un piloto
	@JoinColumn(name="piloto_id")//nombre que se le da a la columna que actua como llave foranea
	private Piloto piloto;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)// un vuelo tiene un avion
	@JoinColumn(name="avion_id")//nombre que se le da a la columna que actua como llave foranea
	private Avion avion;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)// un vuelo tiene un origen
	@JoinColumn(name="aeropuerto_origen_id")//nombre que se le da a la columna que actua como llave foranea
	private Aeropuerto AeropuertoOrigen;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)// un vuelo tiene un destino
	@JoinColumn(name="aeropuerto_destino_id")//nombre que se le da a la columna que actua como llave foranea
	private Aeropuerto AeropuertoDestino;
	
	@Column(name="Estatus")
	private String estatus;

	public Vuelo() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroVuelo() {
		return numeroVuelo;
	}

	public void setNumeroVuelo(String numeroVuelo) {
		this.numeroVuelo = numeroVuelo;
	}

	public Piloto getPiloto() {
		return piloto;
	}

	public void setPiloto(Piloto piloto) {
		this.piloto = piloto;
	}

	public Avion getAvion() {
		return avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	public Aeropuerto getAeropuertoOrigen() {
		return AeropuertoOrigen;
	}

	public void setAeropuertoOrigen(Aeropuerto aeropuertoOrigen) {
		AeropuertoOrigen = aeropuertoOrigen;
	}

	public Aeropuerto getAeropuertoDestino() {
		return AeropuertoDestino;
	}

	public void setAeropuertoDestino(Aeropuerto aeropuertoDestino) {
		AeropuertoDestino = aeropuertoDestino;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	
	
	
	
   
}
