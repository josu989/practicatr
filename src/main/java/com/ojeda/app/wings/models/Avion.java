package com.ojeda.app.wings.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;


@Entity //entidad
@Table(name="aviones") //lo pasamos como tabla
public class Avion {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUST_SEQ2")
	@SequenceGenerator(sequenceName = "customer_seq2", allocationSize = 1, name = "CUST_SEQ2")
	
	@Column(name="id_avion")  //declaramos los atributos y los pasamos como columnas 
	private Long id;
	
	@Column(name="codigo_avion")  //declaramos los atributos y los pasamos como columnas 
	private String codigoAvion;
	
	@Column(name="tipo_avion")  //declaramos los atributos y los pasamos como columnas 
	private String tipoAvion;
	
	@Column(name="horas_vuelo")
	private Integer horaVuelo;
	
	@Column(name="capacidad_pasajeros")
	private Integer capacidadPasajeros;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoAvion() {
		return codigoAvion;
	}

	public void setCodigoAvion(String codigoAvion) {
		this.codigoAvion = codigoAvion;
	}

	public String getTipoAvion() {
		return tipoAvion;
	}

	public void setTipoAvion(String tipoAvion) {
		this.tipoAvion = tipoAvion;
	}

	public Integer getHoraVuelo() {
		return horaVuelo;
	}

	public void setHoraVuelo(Integer horaVuelo) {
		this.horaVuelo = horaVuelo;
	}

	public Integer getCapacidadPasajeros() {
		return capacidadPasajeros;
	}

	public void setCapacidadPasajeros(Integer capacidadPasajeros) {
		this.capacidadPasajeros = capacidadPasajeros;
	}
	//constructor

	public Avion() {
		super();
	}
	
	
	//constructor
	
	
}
