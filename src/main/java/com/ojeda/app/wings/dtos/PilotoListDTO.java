package com.ojeda.app.wings.dtos;

public class PilotoListDTO {
	
	private Long id;
	private String codigo_piloto;
	private String nombre;
	private String ap_paterno;
	private String ap_materno;
	private String genero;
	private Integer horasVuelo;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigo_piloto() {
		return codigo_piloto;
	}
	public void setCodigo_piloto(String codigo_piloto) {
		this.codigo_piloto = codigo_piloto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAp_paterno() {
		return ap_paterno;
	}
	public void setAp_paterno(String ap_paterno) {
		this.ap_paterno = ap_paterno;
	}
	public String getAp_materno() {
		return ap_materno;
	}
	public void setAp_materno(String ap_materno) {
		this.ap_materno = ap_materno;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public Integer getHorasVuelo() {
		return horasVuelo;
	}
	public void setHorasVuelo(Integer horasVuelo) {
		this.horasVuelo = horasVuelo;
	}
	
	
	public PilotoListDTO() {
		super();
	}

	
	
}
