package com.ojeda.app.wings.dtos;

public class AvionListDTO {
	private Long id;
	private String codigoAvion;
	private String tipoAvion;
	private Integer horaVuelo;
	private Integer capacidadPasajeros;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigoAvion() {
		return codigoAvion;
	}
	public void setCodigoAvion(String codigoAvion) {
		this.codigoAvion = codigoAvion;
	}
	public String getTipoAvion() {
		return tipoAvion;
	}
	public void setTipoAvion(String tipoAvion) {
		this.tipoAvion = tipoAvion;
	}
	public Integer getHoraVuelo() {
		return horaVuelo;
	}
	public void setHoraVuelo(Integer horaVuelo) {
		this.horaVuelo = horaVuelo;
	}
	public Integer getCapacidadPasajeros() {
		return capacidadPasajeros;
	}
	public void setCapacidadPasajeros(Integer capacidadPasajeros) {
		this.capacidadPasajeros = capacidadPasajeros;
	}
	
	public AvionListDTO() {
		super();
	}
	
	
	
}
