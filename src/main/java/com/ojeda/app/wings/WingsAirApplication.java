package com.ojeda.app.wings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WingsAirApplication {

	public static void main(String[] args) {
		SpringApplication.run(WingsAirApplication.class, args);
	}

}
