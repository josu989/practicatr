package com.ojeda.app.wings.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ojeda.app.wings.dtos.PilotoCreateDTO;
import com.ojeda.app.wings.dtos.PilotoListDTO;
import com.ojeda.app.wings.services.IService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/pilotos")
@CrossOrigin(origins = "*")
public class PilotosControllers {
	@Autowired
	private IService<PilotoListDTO, PilotoCreateDTO> pilotosService;

	@GetMapping
	public List<PilotoListDTO> getListaPilotos(){
		return pilotosService.getAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<PilotoListDTO> getPilotoById(@PathVariable(name="id") long id){
		return ResponseEntity.ok(pilotosService.getById(id));
	}


	@PostMapping
	public ResponseEntity<PilotoListDTO> guardarPiloto(@Valid @RequestBody PilotoCreateDTO pilotoDTO){
		return new ResponseEntity<>(pilotosService.create(pilotoDTO),HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<PilotoListDTO> actualizarPiloto(@Valid @RequestBody PilotoCreateDTO pilotoDTO, @PathVariable(name="id") long id){
		PilotoListDTO alumnoRespuesta = pilotosService.update(pilotoDTO, id);
		return new ResponseEntity<>(alumnoRespuesta, HttpStatus.NO_CONTENT);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Map<String, String>> eliminarPiloto(@PathVariable(name="id") long id){
		Map<String, String> mapa = new HashMap<>();
		mapa.put("message", "piloto eliminado correctamente");
		pilotosService.delete(id);
		return new ResponseEntity<>(mapa, HttpStatus.OK);
	}

}
