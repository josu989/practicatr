package com.ojeda.app.wings.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ojeda.app.wings.dtos.AvionCreateDTO;
import com.ojeda.app.wings.dtos.AvionListDTO;
import com.ojeda.app.wings.dtos.PilotoCreateDTO;
import com.ojeda.app.wings.dtos.PilotoListDTO;
import com.ojeda.app.wings.services.IService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/aviones")
@CrossOrigin(origins = "*")
public class AvionesController {
	@Autowired
	private IService<AvionListDTO, AvionCreateDTO> avionesService;
//listar aviones
	@GetMapping
	public List<AvionListDTO> getListaAviones(){
		return avionesService.getAll();
	}
	//guardar avion
	@PostMapping
	public ResponseEntity<AvionListDTO> guardarAvion(@Valid @RequestBody AvionCreateDTO AvionDTO){
		return new ResponseEntity<>(avionesService.create(AvionDTO),HttpStatus.CREATED);
	}
}
